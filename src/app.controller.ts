import { Body, Controller, Get, MessageEvent, Post, Sse } from '@nestjs/common';
import { Observable } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { AppService } from './app.service.js';
import { ConversationRequestBody, GenTitleRequestBody } from './app.types';

@Controller("app")
export class AppController {
}

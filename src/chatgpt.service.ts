import { Injectable, Logger, MessageEvent, OnModuleInit } from '@nestjs/common';
import { ChatGPTAPI, ChatMessage, ConversationResponseEvent } from 'chatgpt';
import fetch from './fetch.js';

import { ConfigService } from '@nestjs/config';
import HttpsProxyAgent from 'https-proxy-agent';
import { Observable } from 'rxjs';
import { ErrorMapping, OpenAiConfig } from './config/configuration.types.js';
import path from "path";
import fs from "fs";
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

@Injectable()
export class ChatGPTService implements OnModuleInit {
  private readonly logger = new Logger(ChatGPTService.name);
  private api: ChatGPTAPI;
  //ring array of api
  // private apis: ChatGPTAPI[] = [];
  private apiKeys: string[] = [];
  private proxyAgent: unknown;
  private errorMapping: ErrorMapping[];

  constructor(private readonly configService: ConfigService) {
    this.errorMapping = configService.get('openai.errorMapping') || [];
  }

  onModuleInit() {
    const { HTTP_PROXY, HTTP_PROXY_DOMAIN, HTTP_PROXY_PORT } = process.env;
    if (HTTP_PROXY_DOMAIN && HTTP_PROXY_PORT) {
      this.proxyAgent = HttpsProxyAgent({
        port: HTTP_PROXY_PORT,
        host: HTTP_PROXY_DOMAIN,
        protocol: "https",
        // ca:fs.readFileSync(path.resolve(__dirname,"../ca/ftcp.zhiyoustar.com_other/ftcp.zhiyoustar.com_bundle.crt")),
        // key: fs.readFileSync(path.resolve(__dirname,"../ca/ftcp.zhiyoustar.com_other/ftcp.zhiyoustar.com.key")),
        cert: fs.readFileSync(path.resolve(__dirname, "../ca/ftcp.zhiyoustar.com_other/ftcp.zhiyoustar.com_bundle.crt")),
        rejectUnauthorized: false
      });
    }

    const openaiConfig: OpenAiConfig = this.configService.get('openai') || {};
    const currentDate = (/* @__PURE__ */ new Date()).toISOString().split("T")[0];
    const { systemMessage=`You are GPT-3.5, a large language model trained by OpenAI. Answer as concisely as possible.Knowledge cutoff: 2021-09.Current date: ${currentDate}`, maxTokens, model = 'gpt-3.5-turbo', errorMapping } = openaiConfig;
    this.errorMapping = errorMapping || [];

    this.api = new ChatGPTAPI({
      apiKey: process.env.OPENAI_API_KEY,
      fetch: this.proxyFetch,
      systemMessage,
      maxModelTokens: maxTokens,
      completionParams: {
        model,
      },
    });
    //循环process.env.OPENAI_API_KEYS
    this.apiKeys = process.env.OPENAI_API_KEYS.split(',');
    console.log("当前所有keys", this.apiKeys);
  }

  private i: number = 0;
  private _getApiClient() {
    var length = this.apiKeys.length;
    var apiKey = this.apiKeys[this.i++ % length];
    this.api.apiKey = apiKey;
    console.log("当前key", apiKey);
    return this.api;
  }

  private proxyFetch = (url: string, options?: any) => {
    return fetch(url, {
      ...options,
      agent: this.proxyAgent,
    });
  };

  sendMessage(message: string, parentMessageId: string): Observable<MessageEvent> {
    const observable = new Observable<MessageEvent>((subscriber) => {
      let client = this._getApiClient();
      client.sendMessage(message, {
        parentMessageId,
        onProgress: (partialResponse) => {
          subscriber.next({
            type: 'add',
            data: this.buildMessageEvent(partialResponse),
          });
        },
      })
        .catch((err) => {
          this.logger.error('Error sending message', err, "当前key:", client.apiKey);

          subscriber.next({
            type: 'add',
            data: {
              error: {
                message: this.buildErrorMessage(err),
              },
            },
          });
        })
        .finally(() => {
          subscriber.next({
            data: '[DONE]',
          });
        });
    });

    return observable;
  }

  private buildMessageEvent(chatMessage: ChatMessage): ConversationResponseEvent {
    const { id, role, text } = chatMessage;
    return {
      message: {
        id,
        role,
        user: null,
        create_time: null,
        update_time: null,
        end_turn: null,
        weight: 0,
        recipient: 'all',
        metadata: null,
        content: {
          content_type: 'text',
          parts: [text],
        },
        author: {
          role: 'assistant',
        },
      },
      error: null,
    } as ConversationResponseEvent;
  }

  private buildErrorMessage(err: { message?: string }): string {
    const { message } = err;
    const errorMapping = this.errorMapping.find((item) => message?.includes(item.keyword));
    return "对话出现了问题，刷新网站尝试一下"; //errorMapping?.message || message || 'Unknown error';
  }
}

import { Injectable, NestMiddleware } from '@nestjs/common';
import RateLimit from "express-rate-limit";
@Injectable()
export class ChatapiMiddleware implements NestMiddleware {
  private readonly limiter = RateLimit({
    windowMs:15*60*1000,
    max:100
  })
  use(req: any, res: any, next: () => void) {
    this.limiter(req,res,next)
  }
}

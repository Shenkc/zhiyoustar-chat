import { Module } from '@nestjs/common';
import { ChatapiController } from './chatapi.controller.js';
import { ChatapiService } from './chatapi.service.js';
import { ChatGPTService } from '../chatgpt.service.js';
import { ConfigService } from '@nestjs/config';

@Module({
  controllers: [ChatapiController],
  providers: [ChatapiService,ChatGPTService,ConfigService],
})
export class ChatapiModule {}

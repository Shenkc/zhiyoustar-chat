import { Test, TestingModule } from '@nestjs/testing';
import { ChatapiService } from './chatapi.service';

describe('ChatapiService', () => {
  let service: ChatapiService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [ChatapiService],
    }).compile();

    service = module.get<ChatapiService>(ChatapiService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});

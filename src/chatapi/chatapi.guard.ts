import { CanActivate, ExecutionContext, HttpException, HttpStatus, Injectable, UnauthorizedException, Logger } from '@nestjs/common';
import { Observable } from 'rxjs';
import fetch from '../fetch.js';
import { Headers } from 'node-fetch';
import Cookie from "cookie-parse";
// import { Response } from "express";
@Injectable()
export class ChatapiGuard implements CanActivate {
  private logger = new Logger("ChatapiGuard");
  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    const request = context.switchToHttp().getRequest();
    const cookie = request.headers.cookie && Cookie.parse(request.headers.cookie);
    const ip = request.headers['x-forwarded-for'] && request.headers['x-forwarded-for'].split(",")[0] || request.headers['x-real-ip'] || request.connection.remoteAddress;

    if (!cookie || !cookie.startoken) {
      throw new HttpException({ error: "请先登录", errorCode: HttpStatus.UNAUTHORIZED }, HttpStatus.UNAUTHORIZED)
    } else {
      if (request.body && request.body.messages && request.body.messages.length) {
        let message = request.body.messages[0]?.content.parts[0] ?? '';
        this.logger.log(ip, message)
        if (message.length) {
          return new Promise(async (resolve, reject) => {
            let result = await fetch(process.env.REDIRECT_HTTP + "/wechat/web/chat/conversationRecord", { method: "POST", headers: { cookie: "startoken=" + cookie.startoken, "content-type": "application/json" }, body: JSON.stringify({ content: message, openId: "", ip: ip }) });
            let res: any = await result.json();
            if (res.success) {
              resolve(true);
            } else {
              this.logger.error(res);
              if (["FOLLOW_LOGIN", "LOGIN_ALTER", "RESTART_LOGIN", "TICKET_EXPIRED"].includes(res.errorCode)) {
                reject(new HttpException({ error: "请先登录" }, HttpStatus.UNAUTHORIZED));
              } else {
                reject(new HttpException({ error: "输入异常请重试" }, HttpStatus.FORBIDDEN));
              }
            }
          })
        } else {
          throw new HttpException({ error: "内容不能未空,请开启新对话" }, HttpStatus.FORBIDDEN)
        }
      } else {
        throw new HttpException({ error: "内容不能未空,请开启新对话" }, HttpStatus.FORBIDDEN)
      }
    }


  }
}

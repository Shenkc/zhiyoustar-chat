import { Body, Controller, Get, MessageEvent, Post, Sse, UseGuards, UsePipes } from '@nestjs/common';
import { Observable } from 'rxjs';
import { v4 as uuidv4 } from 'uuid';
import { ChatapiService } from './chatapi.service.js';
import { ConversationRequestBody, GenTitleRequestBody } from '../app.types';
import { ChatapiMiddleware } from './chatapi.middleware.js';
import { ChatapiGuard } from './chatapi.guard.js';

@Controller({ path: '/backend-api' })
// @UsePipes(ChatapiMiddleware)
export class ChatapiController {
  constructor(private readonly chatapiService: ChatapiService) { }
  @Post('moderations')
  moderations() {
    return {
      blocked: false,
      flagged: false,
      moderation_id: uuidv4(),
    };
  }

  @Get('conversations')
  conversations() {
    return {
      items: [],
      total: 0,
      limit: 20,
      offset: 0,
    };
  }

  @Post('conversation')
  @UseGuards(ChatapiGuard)
  @Sse()
  conversation(@Body() body: ConversationRequestBody): Observable<MessageEvent> {
    const { messages, parent_message_id } = body;
    const message = messages[0]?.content.parts[0] ?? '';
    return this.chatapiService.sendMessage(message, parent_message_id);
  }

  @Post('conversation/gen_title/')
  generateTitle(@Body() body: GenTitleRequestBody) {
    return {
      title: '',
    };
  }
}

import { Test, TestingModule } from '@nestjs/testing';
import { ChatapiController } from './chatapi.controller';

describe('ChatapiController', () => {
  let controller: ChatapiController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [ChatapiController],
    }).compile();

    controller = module.get<ChatapiController>(ChatapiController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});

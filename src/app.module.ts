import { Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ServeStaticModule } from '@nestjs/serve-static';
import { join } from 'path';
import { fileURLToPath } from 'url';
import { AppController } from './app.controller.js';
import { AppService } from './app.service.js';
import { ChatapiModule } from './chatapi/chatapi.module.js';
import configuration from './config/configuration.js';
import { EventEmitterModule } from "@nestjs/event-emitter"
const __dirname = fileURLToPath(new URL('.', import.meta.url));

@Module({
  imports: [
    EventEmitterModule.forRoot({
      maxListeners: 150
    }),
    ConfigModule.forRoot({
      load: [configuration],
      envFilePath: process.env.NODE_ENV == 'development' ? 'development.env' : '.env',
      isGlobal: true
    }),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'public'),
      serveRoot: '/',
    }),
    ChatapiModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }

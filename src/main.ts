import { NestFactory } from '@nestjs/core';
import compression from 'compression';
import { AppModule } from './app.module.js';
import { createProxyMiddleware } from "http-proxy-middleware";
import { EventEmitter } from 'events';
import { ChatapiController } from './chatapi/chatapi.controller.js';
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const API_SERVICE_URL = process.env.REDIRECT_HTTP;//process.env.NODE_ENV=='development'? process.env.REDIRECT_HTTP:"http://localhost:8089";
  const maxListeners = Number(process.env.MAX_LISTENERS) || 150
  process.setMaxListeners(maxListeners);
  app.use(compression());
  app.use("/wechat/web", createProxyMiddleware({
    target: API_SERVICE_URL,
    changeOrigin: true,
    pathRewrite: {
      [`^/wechat/web`]: '/wechat/web',
    }
  }))
  app.use("/zy", createProxyMiddleware({
    target: "http://localhost:3001",
    changeOrigin: true,
    pathRewrite: {
      [`^/zy`]: '/zy',
    }
  }))
  await app.listen(3000);
}
bootstrap();
